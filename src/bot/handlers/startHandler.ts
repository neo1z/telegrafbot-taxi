import {Context} from "telegraf";
import {startKeyboard} from "../keyboards/startKeyBoard/startKeyboard";

const startHandler = async (ctx:Context)=>{
    await  ctx.replyWithSticker('https://cdn.tlgrm.app/stickers/637/424/63742453-44b3-30a5-8a97-5ee086adda78/192/11.webp')
    ctx.replyWithHTML("Вас приветствует <b>TaxiBot</b>.\nДля начала работы вам необходимо <b>зарегистрироваться</b>, либо <b>войти в систему</b>!",startKeyboard)
}

export {startHandler}