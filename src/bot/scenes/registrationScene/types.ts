import {Scenes} from "telegraf";

interface IRegistrationSession extends Scenes.WizardSessionData {
    id: string
    email: string
    password:string
    role: 'Таксист' | 'Диспетчер' | 'Заказчик'
    editEmail:boolean
    editPassword:boolean
    editRole:boolean
}

type TRegistrationContext = Scenes.WizardContext<IRegistrationSession>

export {TRegistrationContext}