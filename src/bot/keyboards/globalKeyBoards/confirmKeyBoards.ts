import {Markup} from "telegraf";

const confirmKeyboard = Markup.inlineKeyboard(
    [Markup.button.callback('Да','yes'),
        Markup.button.callback('Нет','not'),])

export {confirmKeyboard}