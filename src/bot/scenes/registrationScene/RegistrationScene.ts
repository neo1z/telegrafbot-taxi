import {Composer, Scenes} from "telegraf";

import {TRegistrationContext} from "./types";
import {confirmRegistration, registrationRoleKeyboard} from "../../keyboards/registrationKeyBoard/registrationKeyboard";
import {UserEntity} from "../../../db/entity/user/user.entity";
import {connectToDB} from "../../../db";


const emailHandler = new Composer<TRegistrationContext>()

const userRepo = connectToDB().then(async (connection)=>{
   return connection.getRepository(UserEntity);
})

emailHandler.on('text',async (ctx)=>{

    const id = String(ctx.message.from.id)

    const messageText = ctx.message.text

    const user =  await (await userRepo).findOne({where:{telegramId:id}})

    if (ctx.scene.session.editEmail){

        if (user && user.email===messageText){
            ctx.reply('Такой пользователь уже есть')
            return ctx.scene.reenter()

        } else {
            ctx.scene.session.editEmail = false
            ctx.scene.session.email = messageText
            await  ctx.reply('Email был изменён')
            ctx.reply(`
Ваш email: ${ctx.scene.session.email}
Ваш пароль ${ctx.scene.session.password}
Ваша роль: ${ctx.scene.session.role}`,confirmRegistration)
            return ctx.wizard.selectStep(3)
        }
    }



    if ( user && user.email === messageText){
        ctx.reply('Такой пользователь уже есть')
       return  ctx.scene.reenter()
    }

    ctx.scene.session.email = messageText
    ctx.scene.session.id = String(id)

    await ctx.reply('Введите пароль')
    return  ctx.wizard.next()
})


const passwordHandler = new Composer<TRegistrationContext>()

passwordHandler.on('text',async (ctx)=>{

    ctx.scene.session.password = ctx.message.text

    if (ctx.scene.session.editPassword){
        ctx.scene.session.editPassword = false

        await ctx.reply(`
Ваш email: ${ctx.scene.session.email} 
Ваш пароль ${ctx.scene.session.password}
Ваша роль: ${ctx.scene.session.role}`,confirmRegistration)
        return ctx.wizard.selectStep(3)
    }
    await ctx.reply('Кем вы являетесь?',registrationRoleKeyboard)

    return ctx.wizard.next()
})


const roleHandler = new Composer<TRegistrationContext>()

roleHandler.action('TaxiDriver',async (ctx)=>{
    ctx.scene.session.role = 'Таксист'
    await ctx.answerCbQuery()

    await ctx.reply(`
Ваш email: ${ctx.scene.session.email} 
Ваш пароль ${ctx.scene.session.password}
Ваша роль: ${ctx.scene.session.role}`,confirmRegistration)

    return ctx.wizard.next()
})

roleHandler.action('Customer',async (ctx)=>{
    await ctx.answerCbQuery()
    ctx.scene.session.role = 'Заказчик'

    await ctx.reply(`
Ваш email: ${ctx.scene.session.email} 
Ваш пароль ${ctx.scene.session.password}
Ваша роль: ${ctx.scene.session.role}`,confirmRegistration)

    return ctx.wizard.next()
})

roleHandler.action('Dispatcher',async (ctx)=>{
    await ctx.answerCbQuery()
    ctx.scene.session.role = 'Диспетчер'

    await ctx.reply(`
Ваш email: ${ctx.scene.session.email} 
Ваш пароль ${ctx.scene.session.password}
Ваша роль: ${ctx.scene.session.role}`,confirmRegistration)

    return ctx.wizard.next()
})

const confirmRegistrationHandler = new Composer<TRegistrationContext>()

confirmRegistrationHandler.action('ConfirmRegistration',async (ctx)=>{
    const{ id,email,password,role } = ctx.scene.session

    const user = new UserEntity()

    user.email = email
    user.telegramId = id
    user.password = password
    user.role = role

    await  (await userRepo).save(user)
    ctx.reply('Вы успешно зарегистрированы')
})


confirmRegistrationHandler.action('ChangeEmail',async (ctx)=>{
    await ctx.answerCbQuery()

    ctx.scene.session.editEmail = true
    ctx.reply('Заново введите логин')
    return ctx.wizard.selectStep(0)
})

confirmRegistrationHandler.action('ChangePassword',async (ctx)=>{
    await ctx.answerCbQuery()

    ctx.scene.session.editPassword = true
    ctx.reply('Заново введите пароль')
    return ctx.wizard.selectStep(1)
})

confirmRegistrationHandler.action('ChangeRole',async (ctx)=>{
    await ctx.answerCbQuery()
    ctx.reply('Заново выберите роль',registrationRoleKeyboard)

    return await ctx.wizard.selectStep(2)
})


const registrationScene = new Scenes.WizardScene<TRegistrationContext>('registration' , emailHandler, passwordHandler,roleHandler, confirmRegistrationHandler)

registrationScene.enter(async(ctx)=> {
    const id = String(ctx.message.from.id)

    const user =  await ( await userRepo).findOne({where:{telegramId:id}})

    if (user){
       await ctx.reply('Вы уже зарегистрированы')
        return ctx.scene.leave()
    }
    await ctx.reply('Введите email')
})


export {registrationScene}