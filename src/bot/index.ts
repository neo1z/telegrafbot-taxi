import "dotenv/config";
import { Telegraf , Scenes ,session } from "telegraf";

import {registrationScene} from "./scenes/registrationScene/RegistrationScene";
import {startHandler} from "./handlers/startHandler";

const bot = new Telegraf<Scenes.WizardContext>(process.env.BOT_TOKEN);

const stage = new Scenes.Stage<Scenes.WizardContext>([registrationScene])

bot.use(session(), stage.middleware())

bot.start(startHandler);

bot.hears('Регистрация' ,async(ctx)=>{
    await ctx.scene.enter('registration')
})

bot.hears('Вход в систему', async (ctx)=>{
    await ctx.scene.enter('signIn')
})

bot.launch().then(()=>{
    console.log('Bot is ready')
});