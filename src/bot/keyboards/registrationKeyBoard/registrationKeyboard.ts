import {Markup} from "telegraf";

const registrationRoleKeyboard = Markup.inlineKeyboard(
    [Markup.button.callback('Таксист','TaxiDriver'),
        Markup.button.callback('Заказчик','Customer'),
        Markup.button.callback('Диспетчер','Dispatcher')])


const confirmRegistration = Markup.inlineKeyboard(
    [[Markup.button.callback('Да, подтверждаю','ConfirmRegistration')],
                    [Markup.button.callback('Изменить email','ChangeEmail')],
                    [Markup.button.callback('Изменить пароль','ChangePassword')],
                    [Markup.button.callback('Изменить роль','ChangeRole')]])



export {registrationRoleKeyboard , confirmRegistration}