import "reflect-metadata";
import { ConnectionOptions, createConnection } from 'typeorm';

import {UserEntity} from "./entity/user/user.entity";

const connectionOptions : ConnectionOptions = {
    name:'default',
    type: "mysql",
    host: process.env.MYSQL_HOST,
    port: 3306,
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    entities: [UserEntity],
    synchronize: true,
    logging: false
}

const connectToDB = async () => await createConnection(connectionOptions);

export {connectionOptions,connectToDB}