import {Entity, Column, PrimaryColumn} from "typeorm";

@Entity()
class UserEntity {
    @PrimaryColumn()
    telegramId: string;

    @Column()
    email:string;

    @Column()
    password:string

    @Column()
    role:string
}

export {UserEntity}