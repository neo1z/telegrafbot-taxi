import {connectToDB} from "../../db";


const createDbHandler = async () => {
    try {
        await connectToDB()
        console.log('Соединение с базой данных создано')

    } catch (error) {
        console.log(error)
    }
}

export {createDbHandler}